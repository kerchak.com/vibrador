document.addEventListener("DOMContentLoaded", function () {
    document.getElementById("vibracion").textContent = "Pulse la pantalla para hacer vibrar el dispositivo.";
  
    document.addEventListener("click", function() {
        var vibracion = navigator.vibrate(500, 200, 300, 200, 100);
        document.getElementById("vibracion").textContent = "Vibración ejecutada con éxito. Refresque la página para repetir el evento.";
    })

})